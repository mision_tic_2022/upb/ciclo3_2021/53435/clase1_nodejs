const express = require('express');
const CarroController = require('../controllers/carroController');

class CarroRouter{

    constructor(){
        this.router = express.Router();
        this.config();
    }

    config(){
        //Crear objeto de tipo CarroController
        const objCarroC = new CarroController();
        //Indicar las rutas
        this.router.post('/carros', objCarroC.crearCarro);
        this.router.get('/carros', objCarroC.obtenerCarros);
        this.router.get('/carros/:placa', objCarroC.obtenerCarroXPlaca);
        this.router.put('/carros/:placa', objCarroC.actualizarCarro);
        this.router.delete('/carros/:placa', objCarroC.eliminarCarro);
    }
}

module.exports = CarroRouter;