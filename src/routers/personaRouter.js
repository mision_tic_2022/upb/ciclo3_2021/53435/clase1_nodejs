const express = require('express');
const PersonaController = require('../controllers/personaController');


class PersonaRouter{

    constructor(){
        this.router = express.Router();
        this.config();
    }

    config(){
        //Crear objeto de tipo PersonaController
        const objPersonaC = new PersonaController();
        this.router.get('/personas', objPersonaC.obtenerPersonas);
        this.router.get('/personas/:cedula', objPersonaC.obtenerPersonaXCedula);
        this.router.post('/personas', objPersonaC.registrarPersona);
        this.router.put('/personas', objPersonaC.actaulizarPersona);
        this.router.delete('/personas', objPersonaC.eliminarPersona);
    }
}

module.exports = PersonaRouter;