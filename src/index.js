const express = require('express');
const CarroRouter = require('./routers/carroRouter');
const PersonaRouter = require('./routers/personaRouter');

class Server {

    constructor() {
        //Crear aplicación express
        this.app = express();
        this.config();
    }

    config() {
        //Indicar/setear el puerto por el que correrá el servidor
        this.app.set('PORT', process.env.PORT || 3000);
        //Indicar que se procesarán datos de las solicitudes en formato json
        this.app.use(express.json());

        //Crear un objeto router
        let router = express.Router();
        router.get('/', (req, res) => {
            res.status(200).send();//.json({message: 'Todo está bien'});
        });
        //Crear rutas
        let objPersonaR = new PersonaRouter();
        let objCarroR = new CarroRouter();

        //Añadir rutas a la app express
        this.app.use(router);
        this.app.use(objPersonaR.router);
        this.app.use(objCarroR.router);
        //Levantar servidor / poner el servidor a la escucha
        this.app.listen(this.app.get('PORT'), () => {
            console.log("Servidor corriendo por el puerto  =>  ", this.app.get('PORT'));
        });
    }

}

new Server();