const Carro = require('../models/carro');
//Arreglo global que almacena objetos de tipo Carro
var carros = [];

/***************************************
 * Desarrolle los end points para las 
 * funcionalidades del CRUD de un carro;
 * Crear un carro
 * Obtener todos los carros
 * Obtener carro por placa
 * Actualizar carro por placa
 * Eliminar carro por placa
 * Todo haciendo uso del modelo Carro ubicado 
 * en el paquete 'models'
 ***************************************/

class CarroController {

    constructor() {

    }

    crearCarro(req, res) {
        //Capturar datos del cuerpo de la petición
        let { placa, color, modelo } = req.body;
        //Crear un objeto con los datos capturados
        let objCarro = new Carro(placa, color, modelo);
        //añadir al arrreglo
        carros.push(objCarro);
        res.status(201).send();
    }

    obtenerCarros(req, res) {
        res.status(200).json(carros);
    }

    obtenerCarroXPlaca(req, res) {
        //Capturar placa de la url de la petición
        let placa = req.params.placa;
        //Filtrar carro por placa
        let carro = carros.filter(objCarro => (objCarro.placa == placa));
        if (carro.length > 0) {
            res.status(200).json(carro);
        } else {
            res.status(200).json({ message: "Carro no encontrado" });
        }
    }

    actualizarCarro(req, res) {
        let placa = req.params.placa;
        let { color, modelo } = req.body;
        if (placa != null && color != null && modelo != null) {
            carros.map(objCarro => {
                if (objCarro.placa == placa) {
                    objCarro.color = color;
                    objCarro.modelo = modelo;
                }
            });
            res.status(200).send();
        } else {
            res.status(400).send();
        }
    }

    eliminarCarro(req, res){
        let placa = req.params.placa;
        let temporal = carros.filter(objCarro => (objCarro.placa != placa));
        carros = temporal;
        res.status(200).send();
    }


}

module.exports = CarroController;