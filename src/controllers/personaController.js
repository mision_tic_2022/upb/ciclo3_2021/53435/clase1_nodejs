var personas = [
    { nombre: "Andrés", apellido: "Quintero", telefono: "123456", cedula: "98765" },
    { nombre: "Juan", apellido: "Perez", telefono: "43109", cedula: "123455" },
    { nombre: "Ana Carolina ", apellido: "Pinzón", telefono: "478921", cedula: "102986" },
    { nombre: "Camilo", apellido: "Hernández", telefono: "4789", cedula: "135427" },
    { nombre: "Juliana", apellido: "Perea", telefono: "6432111", cedula: "11223456" },
    { nombre: "Julián", apellido: "Román", telefono: "46789", cedula: "76421" },
    { nombre: "René", apellido: "Higuita", telefono: "5489999", cedula: "90856" },
    { nombre: "Andrés", apellido: "Escobar", telefono: "9999111", cedula: "365321" }
]


class PersonaController {

    obtenerPersonas(req, res) {
        res.status(200).json(personas);
    }

    /*
    Ejercicio: Construir un endpoint de tipo get que retorne los datos de una
    persona por cédula (deberá obtener el parámetro cédula)
     */
    obtenerPersonaXCedula(req, res){
        //Capturar parámetros de la url
        let cedula = req.params.cedula;
        //Buscar persona por cédula
        let objPersona = personas.filter( obj =>  (obj.cedula == cedula));
        if(objPersona.length > 0){
            res.status(200).json(objPersona[0]);
        }else{
            res.status(200).json({message: "Persona no encontrada"});
        }
    }

    registrarPersona(req, res) {
        //Capturar los datos del cuerpo de la petición
        let { nombre, apellido, telefono, cedula } = req.body;
        //Imprimir datos por consola
        console.log(nombre);
        console.log(cedula);
        console.log(req.body);
        //Ejercicio:
        //Validar que ningún dato sea nulo
        //En caso de tener algún dato nulo retornar un status 400
        if (nombre != null && apellido != null && telefono != null && cedula != null) {
            //Construcción de objeto js
            let objPersona = {
                nombre,
                apellido,
                telefono,
                cedula
            }
            //añadir el objeto al arreglo global
            personas.push(objPersona);
            //Responser la solicitud al cliente
            res.status(201).send();
        }else{
            res.status(400).send();
        }
    }

    actaulizarPersona(req, res){
        let {nombre, apellido, telefono, cedula} = req.body;
        console.log(nombre);
        console.log(cedula);

        /**
         * Ejercicio:
         * Buscar una persona por cédula y actualizar el nombre, apellido y teléfono
         */
        personas.map(objPersona => {
            if(objPersona.cedula == cedula){
                objPersona.nombre = nombre;
                objPersona.apellido = apellido;
                objPersona.telefono = telefono;
            }
        });

        res.status(200).send();
    }

    eliminarPersona(req, res){
        /**
         * Ejercicio:
         * Eliminar una persona por cédula
         */
        let { cedula } = req.body;
        let temporal = personas.filter( objPersonas =>  (objPersonas.cedula != cedula));
        personas = temporal;
        res.status(200).send();
    }

}

module.exports = PersonaController;