class Carro{

    constructor(placa, color, modelo){
        this.placa = placa;
        this.color = color;
        this.modelo = modelo;
    }

}

module.exports = Carro;